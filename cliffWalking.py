import pygame
import numpy as np
import time
import random
import gym
import pickle
import os

# Defining grid tiles
BLACK = (0, 0, 0)
TILE_VALUES = {
    0: (0, 255, 0),  # Grass
    1: (0, 0, 255),  # Water
    2: (0, 0, 0),  # Player
    3: (255, 0, 0),  # Goal
}

# Grid tile sizes in pixels
WIDTH = 50
HEIGHT = 50
MARGIN = 5

# Number of Tiles in grid
GRIDSIZE = 6

MAX_STEPS = 128

SCREEN_SIZE = (((WIDTH + MARGIN) * GRIDSIZE), ((WIDTH + MARGIN) * GRIDSIZE))

# Chance of Wind in percent, can be manually set in constructor
CHANCE_OF_WIND = 0

# Reward values
REWARD_LOST = -1
REWARD_WON = 1
REWARD_STEPS = 0  # must be 0 or negative otherwise positive rewards can be accumulated for running a long time


class CliffGame(object):

    # defining functions
    def __init__(self, render=False, chance_of_wind=CHANCE_OF_WIND, random_grid=False, test_grid=False, wind_strength=1,
                 log=False, reward_won=REWARD_WON, reward_lost=REWARD_LOST, name=""):
        self.random_grid = random_grid
        self.test_grid = test_grid
        self._init_grid()
        self.game_over = False
        self.reward = 0
        self.reward_won = reward_won
        self.reward_lost = reward_lost
        self.render = render
        self.chance_of_wind = chance_of_wind
        self.wind_strength = wind_strength
        pygame.init()
        self.screen = pygame.display.set_mode(SCREEN_SIZE)
        pygame.display.set_caption("Cliff Walk")
        pygame.event.pump()
        self.clock = pygame.time.Clock()
        self.plot = log
        self.name = name
        self.wind_block = 0

        # Data to save per step, coordinates etc.
        self.steps = 0
        self.distance_to_goal_arr = []
        self.distance_to_cliff_arr = []
        self.distance_to_cliff = []
        distance_to_goal = self.get_distance_to_goal()
        self.distance_to_goal_arr.append(distance_to_goal)
        distance_to_cliff = self.get_distance_to_cliff()
        self.distance_to_cliff.append(distance_to_cliff)
        self.coordinate_arr = []
        self.coordinate_arr.append((self.player_y, self.player_x))

        # Data to save per episode
        self.wind_occurrences = 0
        self.episode = 0

        # Arrays for saving data over episodes
        self.wind_occurrences_arr = []
        self.steps_arr = []
        self.success_arr = []

        # for gym compatibility
        self.action_space = gym.spaces.Discrete(4)
        self.observation_space = gym.spaces.Box(low=0, high=3, shape=(6, 6))
        self.reward_range = [self.reward_lost, self.reward_won]
        self.metadata = {'render.modes': ['ansi']}

    def step(self, direction, wind=True):
        """
        Moving the player in the given direction and performing the wind move
        0: left
        1: right
        2: top
        3: bottom
        """
        old_player_x = self.player_x
        old_player_y = self.player_y

        self.steps += 1

        if direction == 0 and self.player_x > 0:
            self.player_x -= 1
        elif direction == 1 and self.player_x < GRIDSIZE - 1:
            self.player_x += 1
        elif direction == 2 and self.player_y > 0:
            self.player_y -= 1
        elif direction == 3 and self.player_y < GRIDSIZE - 1:
            self.player_y += 1

        # Update grid after taking a step
        self.calculate_reward()
        self.update_grid(old_player_x, old_player_y)
        self.coordinate_arr.append((self.player_y, self.player_x))

        # log distance to goal and to cliff
        distance_to_cliff = self.get_distance_to_cliff()
        self.distance_to_cliff.append(distance_to_cliff)
        distance_to_goal = self.get_distance_to_goal()
        self.distance_to_goal_arr.append(distance_to_goal)

        # Move player by wind
        if self.wind_block <= 0 and wind and np.random.uniform(0, 100) < self.chance_of_wind:
            self.wind_occurrences += 1
            for _ in range(self.wind_strength):
                if not self.game_over:
                    if self.wind_strength > 1:
                        self.wind_block = 2
                    self.step(self.wind_direction, wind=False)

        if wind and self.wind_strength > 1:
            self.wind_block -= 1

        if self.render:
            time.sleep(0.2)
            self.draw_screen()

        return self.grid, self.reward, self.game_over, {}

    def draw_screen(self):
        if self.game_over:
            return 0
        # Reset screen
        self.screen.fill(BLACK)
        # Draw screen
        for row in range(GRIDSIZE):
            for column in range(GRIDSIZE):
                color = TILE_VALUES[self.grid[row][column]]
                pygame.draw.rect(self.screen,
                                 color,
                                 [(MARGIN + WIDTH) * column + MARGIN,
                                  (MARGIN + HEIGHT) * row + MARGIN,
                                  WIDTH,
                                  HEIGHT])
        # Update screen
        pygame.display.flip()

    def update_grid(self, old_player_x, old_player_y):
        self.grid[old_player_y][old_player_x] = 0
        self.grid[self.player_y][self.player_x] = 2

    def calculate_reward(self):
        """
        calculating the reward based on the distance to the goal
        """
        # Rewards should not be too big or low, try smaller scales and normalizing
        # distance = self.get_distance_to_goal()
        # self.reward = REWARD_STEPS - (distance / 10)
        self.reward = REWARD_STEPS
        if self.player_x in self.cliff_x and self.player_y in self.cliff_y:
            self.reward = self.reward_lost
            self.success_arr.append(0)
            self.game_over = True
        elif self.player_x == self.goal_x and self.player_y == self.goal_y:
            self.reward = self.reward_won
            self.success_arr.append(1)
            self.game_over = True
        elif self.steps >= MAX_STEPS:
            self.success_arr.append(0)
            self.game_over = True

    def reset(self):
        # TODO: add logging after each episode in here
        if self.plot:
            self.log_values()
        # Initializing grid and resetting game values
        self._init_grid()
        self.game_over = False
        self.reward = 0
        self.steps = 0

        self.distance_to_cliff = []
        self.distance_to_cliff.append(self.get_distance_to_cliff())

        # resetting values for plotting
        self.wind_occurrences = 0
        self.coordinate_arr.append((self.player_y, self.player_x))

        return self.grid

    def close(self):
        if self.plot:
            self.log_values()
            self.save_plots()
        pygame.quit()

    def get_reward(self):
        return self.reward

    def play(self):
        done = False
        steps_taken = 0

        while not done:
            # Game logic
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    done = True
                elif event.type == pygame.KEYDOWN:
                    # Move player in direction he pressed
                    if event.key == pygame.K_LEFT:
                        self.step(0)
                    elif event.key == pygame.K_RIGHT:
                        self.step(1)
                    elif event.key == pygame.K_UP:
                        self.step(2)
                    elif event.key == pygame.K_DOWN:
                        self.step(3)

                    done = self.game_over

            # Draw grid
            self.draw_screen()

            # Limit to 30 fps
            self.clock.tick(30)

        # pygame.quit()

    def _init_grid(self):
        """
        Initializing the grid, cliff can be at all four sides with the goal and player
        on opposite sites of the cliff, giving the direction of the wind in the
        """
        # Initializing grid of Grass
        self.grid = []
        for row in range(GRIDSIZE):
            self.grid.append([])
            for column in range(GRIDSIZE):
                self.grid[row].append(0)

        if not self.random_grid and not self.test_grid:
            # Initializing Cliff
            for row in range(1, GRIDSIZE - 1):
                self.grid[row][GRIDSIZE - 1] = 1
            self.cliff_x = [GRIDSIZE - 1]
            self.cliff_y = range(1, GRIDSIZE-1)
            # Initializing start of player (2) and goal (3)
            self.grid[GRIDSIZE - 1][GRIDSIZE - 1] = 2
            self.grid[0][GRIDSIZE - 1] = 3
            self.player_x = GRIDSIZE - 1
            self.player_y = GRIDSIZE - 1
            self.goal_x = GRIDSIZE - 1
            self.goal_y = 0
            self.wind_direction = 1
        if self.random_grid:
            # Deciding on side of cliff
            sides = [0, 1, 2]
            side = random.choice(sides)
            self.wind_direction = side

            if side == 1:
                # Deciding on placement of player and goal
                places = [2, 3]
                player_start = random.choice(places)

                # Initializing Cliff
                for row in range(1, GRIDSIZE - 1):
                    self.grid[row][GRIDSIZE - 1] = 1
                self.cliff_x = [GRIDSIZE - 1]
                self.cliff_y = range(1, GRIDSIZE-1)
                # Initializing start of player (2) and goal (3)
                if player_start == 3:
                    self.grid[GRIDSIZE - 1][GRIDSIZE - 1] = 2
                    self.grid[0][GRIDSIZE - 1] = 3
                    self.player_x = GRIDSIZE - 1
                    self.player_y = GRIDSIZE - 1
                    self.goal_x = GRIDSIZE - 1
                    self.goal_y = 0
                else:
                    self.grid[GRIDSIZE - 1][GRIDSIZE - 1] = 3
                    self.grid[0][GRIDSIZE - 1] = 2
                    self.player_x = GRIDSIZE - 1
                    self.player_y = 0
                    self.goal_x = GRIDSIZE - 1
                    self.goal_y = GRIDSIZE - 1

            if side == 0:
                # Deciding on placement of player and goal
                places = [2, 3]
                player_start = random.choice(places)

                # Initializing Cliff
                for row in range(1, GRIDSIZE - 1):
                    self.grid[row][0] = 1
                self.cliff_x = [0]
                self.cliff_y = range(1, GRIDSIZE-1)
                # Initializing start of player (2) and goal (3)
                if player_start == 3:
                    self.grid[GRIDSIZE - 1][0] = 2
                    self.grid[0][0] = 3
                    self.player_x = 0
                    self.player_y = GRIDSIZE - 1
                    self.goal_x = 0
                    self.goal_y = 0
                else:
                    self.grid[GRIDSIZE - 1][0] = 3
                    self.grid[0][0] = 2
                    self.player_x = 0
                    self.player_y = 0
                    self.goal_x = 0
                    self.goal_y = GRIDSIZE - 1

            if side == 2:
                # Deciding on placement of player and goal
                places = [0, 1]
                player_start = random.choice(places)

                # Initializing Cliff
                for column in range(1, GRIDSIZE - 1):
                    self.grid[0][column] = 1
                self.cliff_x = range(1, GRIDSIZE-1)
                self.cliff_y = [0]
                # Initializing start of player (2) and goal (3)
                if player_start == 0:
                    self.grid[0][0] = 2
                    self.grid[0][GRIDSIZE - 1] = 3
                    self.player_x = 0
                    self.player_y = 0
                    self.goal_x = GRIDSIZE - 1
                    self.goal_y = 0
                else:
                    self.grid[0][0] = 3
                    self.grid[0][GRIDSIZE - 1] = 2
                    self.player_x = GRIDSIZE - 1
                    self.player_y = 0
                    self.goal_x = 0
                    self.goal_y = 0

        if self.test_grid:
            # Deciding on placement of player and goal
            places = [0, 1]
            player_start = random.choice(places)
            self.wind_direction = 3

            # Initializing Cliff
            for column in range(1, GRIDSIZE - 1):
                self.grid[GRIDSIZE - 1][column] = 1
            self.cliff_x = range(1, GRIDSIZE-1)
            self.cliff_y = [GRIDSIZE - 1]
            # Initializing start of player (2) and goal (3)
            if player_start == 0:
                self.grid[GRIDSIZE - 1][0] = 2
                self.grid[GRIDSIZE - 1][GRIDSIZE - 1] = 3
                self.player_x = 0
                self.player_y = GRIDSIZE - 1
                self.goal_x = GRIDSIZE - 1
                self.goal_y = GRIDSIZE - 1
            else:
                self.grid[GRIDSIZE - 1][0] = 3
                self.grid[GRIDSIZE - 1][GRIDSIZE - 1] = 2
                self.player_x = GRIDSIZE - 1
                self.player_y = GRIDSIZE - 1
                self.goal_x = 0
                self.goal_y = GRIDSIZE - 1

    def log_values(self):
        self.episode += 1
        self.distance_to_cliff_arr.append(sum(self.distance_to_cliff) / len(self.distance_to_cliff))
        self.wind_occurrences_arr.append(self.wind_occurrences)
        self.steps_arr.append(self.steps)

        return 0

    def save_plots(self):
        dict_of_plots = {
            "wind_occurrences_per_episode": self.wind_occurrences_arr,
            "steps_per_episode": self.steps_arr,
            "success_per_episode": self.success_arr,
            "coordinates": self.coordinate_arr,
            "distance_to_cliff": self.distance_to_cliff_arr,
            "distance_to_goal": self.distance_to_goal_arr,
        }
        if not os.path.exists("pickles"):
            os.makedirs("pickles")
        file_name = open(f"pickles/{self.name}.pkl", "wb")
        pickle.dump(dict_of_plots, file_name)

    def get_distance_to_goal(self):
        distance = (abs(self.goal_y-self.player_y) + abs(self.goal_x-self.player_x))
        return distance

    def get_distance_to_cliff(self):
        y = 5
        x = 5
        for i in self.cliff_y:
            if abs(self.player_y - i) < y:
                y = abs(self.player_y - i)
        for i in self.cliff_x:
            if abs(self.player_x - i) < x:
                x = abs(self.player_x - i)

        distance = x + y
        return distance


if __name__ == '__main__':
    cliffgame = CliffGame(render=True)
    for i in range(1):
        cliffgame.reset()
        cliffgame.play()

    cliffgame.close()
