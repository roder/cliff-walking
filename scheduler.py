from stableDQN import DQNAgent
from stablePPO import PPOAgent


if __name__ == '__main__':

    for i in range(15):
        print(f"PPO, random_grid {i+1}/15")
        name = f"PPO_random_6_f8_{i+1}"
        agent = PPOAgent(chance_of_wind=17, random_grid=True, reward_won=1, reward_lost=-1, name=name)
        agent.learn(1_000_000)
        agent.evaluate_agent(200)
        agent.evaluate_agent_no_wind(200)
        agent.evaluate_agent_test_grid(200)
        agent.evaluate_agent_test_grid_no_wind(200)
        print("------------------------------")
