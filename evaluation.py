import cliffWalking

from stable_baselines3 import PPO
from stable_baselines3 import DQN
from stable_baselines3.common.evaluation import evaluate_policy


class Evaluator(object):
    def __init__(self, models, chance_of_wind=17, wind_strength=1, ppo=False, dqn=False):
        self.models = models
        self.ppo = ppo
        self.dqn = dqn
        self.chance_of_wind = chance_of_wind
        self.wind_strength = wind_strength
        if self.ppo and self.dqn:
            print("Warning, do not use PPO and DQN in the same evaluation")
        if self.ppo:
            print("PPO Evaluator...")
        if self.dqn:
            print("DQN Evaluator...")

    def evaluate_wind(self, episodes):
        for model in self.models:
            print(f"Evaluating model {model} with {self.chance_of_wind}% wind chance, {self.wind_strength}x wind")
            name = f"{model}_EVAL_wind"
            env = cliffWalking.CliffGame(chance_of_wind=self.chance_of_wind, wind_strength=self.wind_strength,
                                         log=True, name=name)
            if self.ppo:
                agent = PPO.load(f"ModelsPPO/{model}.zip", device='auto')
            if self.dqn:
                agent = DQN.load(f"ModelsStableDQN/{model}.zip", device='auto')

            mean_reward, std_reward = evaluate_policy(agent, env, n_eval_episodes=episodes)
            env.close()
            print(f"Evaluation complete, mean reward was {mean_reward} +/- {std_reward}")
            print(f"Plots of evaluation saved to pickles/{name}")
            print("-----------------------------------------------------")

    def evaluate_no_wind(self, episodes):
        for model in self.models:
            print(f"Evaluating model {model} with 0% wind chance")
            name = f"{model}_EVAL_no_wind"
            env = cliffWalking.CliffGame(chance_of_wind=0, log=True, name=name)
            if self.ppo:
                agent = PPO.load(f"ModelsPPO/{model}.zip", device='auto')
            if self.dqn:
                agent = DQN.load(f"ModelsStableDQN/{model}.zip", device='auto')

            mean_reward, std_reward = evaluate_policy(agent, env, n_eval_episodes=episodes)
            env.close()
            print(f"Evaluation complete, mean reward was {mean_reward} +/- {std_reward}")
            print(f"Plots of evaluation saved to pickles/{name}")
            print("-----------------------------------------------------")

    def evaluate_random_grid(self, episodes):
        for model in self.models:
            print(f"Evaluating model {model} with {self.chance_of_wind}% wind chance and "
                  f"random_grid=True")
            name = f"{model}_EVAL_random_grid"
            env = cliffWalking.CliffGame(chance_of_wind=self.chance_of_wind, log=True, name=name, random_grid=True)
            if self.ppo:
                agent = PPO.load(f"ModelsPPO/{model}.zip", device='auto')
            if self.dqn:
                agent = DQN.load(f"ModelsStableDQN/{model}.zip", device='auto')

            mean_reward, std_reward = evaluate_policy(agent, env, n_eval_episodes=episodes)
            env.close()
            print(f"Evaluation complete, mean reward was {mean_reward} +/- {std_reward}")
            print(f"Plots of evaluation saved to pickles/{name}")
            print("-----------------------------------------------------")

    def evaluate_test_grid(self, episodes):
        for model in self.models:
            print(f"Evaluating model {model} with {self.chance_of_wind}% wind chance and "
                  f"test_grid=True")
            name = f"{model}_EVAL_test_grid"
            env = cliffWalking.CliffGame(chance_of_wind=self.chance_of_wind, log=True, name=name, test_grid=True)
            if self.ppo:
                agent = PPO.load(f"ModelsPPO/{model}.zip", device='auto')
            if self.dqn:
                agent = DQN.load(f"ModelsStableDQN/{model}.zip", device='auto')

            mean_reward, std_reward = evaluate_policy(agent, env, n_eval_episodes=episodes)
            env.close()
            print(f"Evaluation complete, mean reward was {mean_reward} +/- {std_reward}")
            print(f"Plots of evaluation saved to pickles/{name}")
            print("-----------------------------------------------------")


if __name__ == '__main__':
    models = [

        "DQN_reward_1-10_1",
        "DQN_reward_1-10_2",
        "DQN_reward_1-10_3",
        "DQN_reward_1-10_4",
        "DQN_reward_1-10_5",
        "DQN_reward_1-10_6",
        "DQN_reward_1-10_7",
        "DQN_reward_1-10_8",
        "DQN_reward_1-10_9",
        "DQN_reward_1-10_10",
        "DQN_reward_1-10_11",
        "DQN_reward_1-10_12",

    ]

    evaluator = Evaluator(models, chance_of_wind=17, wind_strength=1, dqn=True)

    evaluator.evaluate_wind(200)
    evaluator.evaluate_no_wind(200)
