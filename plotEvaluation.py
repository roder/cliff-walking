import matplotlib.pyplot as plt
import pickle
import numpy as np

plt.rc('font', size=13)
plt.rc('axes', titlesize=11)
plt.rc('axes', labelsize=13)
plt.rc('xtick', labelsize=11)
plt.rc('ytick', labelsize=11)


class Data(object):
    def __init__(self, paths):

        self.numpaths = len(paths)

        self.wind_occurrences_arr = []
        self.steps_arr = []
        self.success_arr = []
        self.coordinates = []
        self.distance_to_cliff_arr = []
        self.min = 0

        for i in range(self.numpaths):
            file_name = open(f"pickles/{paths[i]}.pkl", "rb")
            dict_of_plots = pickle.load(file_name)
            self.wind_occurrences_arr.append(dict_of_plots["wind_occurrences_per_episode"])
            self.steps_arr.append(dict_of_plots["steps_per_episode"])
            self.success_arr.append(dict_of_plots["success_per_episode"])
            self.coordinates.append(dict_of_plots["coordinates"])
            self.distance_to_cliff_arr.append(dict_of_plots["distance_to_cliff"])

        self.wind_occurrences_arr = self.combine_arrays(self.cut_arrays(self.wind_occurrences_arr))
        self.steps_arr = self.combine_arrays(self.cut_arrays(self.steps_arr))
        self.distance_to_cliff_arr = self.combine_arrays(self.cut_arrays(self.distance_to_cliff_arr))
        self.success_arr = self.combine_arrays(self.cut_arrays(self.success_arr))

    def combine_arrays(self, array):
        avg_array = np.average(array, axis=0)

        return avg_array

    def cut_arrays(self, array):
        if self.numpaths > 1:
            minim = len(array[0])
            for x in array:
                if len(x) < minim:
                    minim = len(x)

            for i in range(len(array)):
                array[i] = array[i][:minim]

            self.min = minim
        return array


class EvaluationPlotter(object):
    def __init__(self, paths, titles=False):
        self.paths = paths
        self.num_of_plots = len(paths)

        self.titles = titles

        self.logs = []
        self.labels = []
        if self.num_of_plots > 2:
            self.colors = [
                "steelblue",
                "royalblue",
                "navy",
                "sandybrown",
                "orange",
                "darkorange"
            ]
        else:
            self.colors = [
                "steelblue",
                "sandybrown"
            ]
        for i in range(self.num_of_plots):
            data = Data(paths[i][1])
            self.logs.append(data)
            self.labels.append(self.paths[i][0])

    def boxplot_wind(self):
        fig, ax = plt.subplots()

        data = []
        for log in self.logs:
            data.append(log.wind_occurrences_arr)

        ax.boxplot(data)
        ax.yaxis.grid(True, linestyle='-', which='both', color='lightgrey', alpha=0.5)
        ax.set_xticklabels(self.labels)
        ax.set_ylabel("Windhäufigkeit")

        ax.set_ylim(bottom=0)
        ax.set_xlim(left=0)

    def boxplot_steps(self):
        fig, ax = plt.subplots()

        data = []
        for log in self.logs:
            data.append(log.steps_arr)

        ax.boxplot(data)
        ax.yaxis.grid(True, linestyle='-', which='both', color='lightgrey', alpha=0.5)
        ax.set_xticklabels(self.labels)
        ax.set_ylabel("Schritte")

        ax.set_ylim([0, 17.5])
        ax.set_xlim(left=0)

    def boxplot_success(self):
        fig, ax = plt.subplots()

        data = []
        for log in self.logs:
            data.append(log.success_arr)

        ax.boxplot(data)
        ax.yaxis.grid(True, linestyle='-', which='both', color='lightgrey', alpha=0.5)
        ax.set_xticklabels(self.labels)
        ax.set_ylabel("Erfolg")

        ax.set_ylim([0, 1])
        ax.set_xlim(left=0)

    def boxplot_distance(self):
        fig, ax = plt.subplots()

        data = []
        for log in self.logs:
            data.append(log.distance_to_cliff_arr)

        ax.boxplot(data)
        ax.yaxis.grid(True, linestyle='-', which='both', color='lightgrey', alpha=0.5)
        ax.set_xticklabels(self.labels)
        ax.set_ylabel("Distanz zur Klippe")

        ax.set_ylim([0, 2.5])
        ax.set_xlim(left=0)

    def barchart_success(self):
        fig, ax = plt.subplots(figsize=(7, 4.8))

        means = []
        stds = []
        for log in self.logs:
            means.append(np.mean(log.success_arr))
            stds.append(np.std(log.success_arr))

        x_pos = np.arange(self.num_of_plots)

        ax.set_axisbelow(True)
        ax.yaxis.grid(True, linestyle='-', which='both', color='lightgrey', alpha=1)

        ax.bar(x_pos, means, width=0.5, color=self.colors, yerr=stds, capsize=5)

        ax.set_xticks(x_pos)
        ax.set_xticklabels(self.labels)
        ax.set_ylabel("Erfolg")

        if self.num_of_plots <= 2:
            ax.set_xlim(-0.7, 1.7)
        ax.set_ylim(0, 1.2)

        fig.tight_layout()

    def barchart_distance(self):
        fig, ax = plt.subplots(figsize=(7, 4.8))

        means = []
        stds = []
        for log in self.logs:
            means.append(np.mean(log.distance_to_cliff_arr))
            stds.append(np.std(log.distance_to_cliff_arr))

        x_pos = np.arange(self.num_of_plots)

        ax.set_axisbelow(True)
        ax.yaxis.grid(True, linestyle='-', which='both', color='lightgrey', alpha=1)

        ax.bar(x_pos, means, width=0.5, color=self.colors, yerr=stds, capsize=5)

        ax.set_xticks(x_pos)
        ax.set_xticklabels(self.labels)
        ax.set_ylabel("Distanz zur Klippe")

        if self.num_of_plots <= 2:
            ax.set_xlim(-0.7, 1.7)
        ax.set_ylim(0, 3)

        fig.tight_layout()

    def barchart_steps(self):
        fig, ax = plt.subplots(figsize=(7, 4.8))

        means = []
        stds = []
        for log in self.logs:
            means.append(np.mean(log.steps_arr))
            stds.append(np.std(log.steps_arr))

        x_pos = np.arange(self.num_of_plots)

        ax.set_axisbelow(True)
        ax.yaxis.grid(True, linestyle='-', which='both', color='lightgrey', alpha=1)

        ax.bar(x_pos, means, width=0.5, color=self.colors, yerr=stds, capsize=5)

        ax.set_xticks(x_pos)
        ax.set_xticklabels(self.labels)
        ax.set_ylabel("Schritte pro Episode")

        if self.num_of_plots <= 2:
            ax.set_xlim(-0.7, 1.7)
        ax.set_ylim(bottom=0)

        fig.tight_layout()


if __name__ == '__main__':
    paths = [
    ]
    plotter = EvaluationPlotter(paths)
    plotter.barchart_success()
    plotter.barchart_distance()
    plotter.barchart_steps()
    plotter.boxplot_success()
    plotter.boxplot_wind()
    plotter.boxplot_steps()
    plotter.boxplot_distance()
