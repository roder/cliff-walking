import matplotlib.pyplot as plt
import pickle
from pywaffle import Waffle


class Depickle(object):
    def __init__(self, paths):

        self.numpaths = len(paths)

        self.wind_occurrences_arr = []
        self.steps_arr = []
        self.success_arr = []
        self.coordinates = []
        self.distance_to_cliff_arr = []
        self.min = 0

        for i in range(self.numpaths):
            file_name = open(f"pickles/{paths[i]}.pkl", "rb")
            dict_of_plots = pickle.load(file_name)
            self.wind_occurrences_arr.append(dict_of_plots["wind_occurrences_per_episode"])
            self.steps_arr.append(dict_of_plots["steps_per_episode"])
            self.success_arr.append(dict_of_plots["success_per_episode"])
            self.coordinates.append(dict_of_plots["coordinates"])
            self.distance_to_cliff_arr.append(dict_of_plots["distance_to_cliff"])


class WaffleChart(object):
    def __init__(self, paths):
        self.paths = paths
        self.num_of_plots = len(paths)

        self.logs = []
        self.labels = []

        for i in range(self.num_of_plots):
            data = Depickle(paths[i][1])
            self.logs.append(data)
            self.labels.append(self.paths[i][0])

    def waffle_chart(self):
        rewards = []
        i = 0
        for log in self.logs:
            reward_dict = {
                -1: 0,
                0: 0,
                1: 0
            }
            for success in log.success_arr:
                for i in success:
                    reward_dict[i] += 1
            total = sum(reward_dict.values(), 0.0)
            reward_dict = {k: (float("{:.2f}".format(100 * v / total))) for k, v in reward_dict.items()}
            rewards.append(reward_dict)

        for reward_dict in rewards:
            fig = plt.figure(
                FigureClass=Waffle,
                rows=10,
                interval_ratio_x=0.1,
                interval_ratio_y=0.1,
                block_aspect_ratio=1.3,
                values=reward_dict,
                cmap_name="Pastel1",
                labels=[f"Belohnung {k}\n({v}%)" for k, v in reward_dict.items()],
                legend={'loc': 'lower left', 'bbox_to_anchor': (0.05, -0.14), 'ncol': 3, 'fontsize': 12},
                figsize=(6.7, 6)
            )

            plt.show()


if __name__ == '__main__':
    paths = [
        ["DQN", ["DQN_random_6of8_1_EVAL_test_grid", "DQN_random_6of8_2_EVAL_test_grid", "DQN_random_6of8_3_EVAL_test_grid", "DQN_random_6of8_4_EVAL_test_grid",
                 "DQN_random_6of8_5_EVAL_test_grid", "DQN_random_6of8_6_EVAL_test_grid", "DQN_random_6of8_7_EVAL_test_grid", "DQN_random_6of8_8_EVAL_test_grid",
                 "DQN_random_6of8_9_EVAL_test_grid", "DQN_random_6of8_10_EVAL_test_grid", "DQN_random_6of8_11_EVAL_test_grid", "DQN_random_6of8_12_EVAL_test_grid"]],
        ["PPO",
         ["PPO_random_6of8_15_EVAL_test_grid"]]
    ]

    wafflechart = WaffleChart(paths)
    wafflechart.waffle_chart()
