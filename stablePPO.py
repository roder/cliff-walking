import cliffWalking
import torch
from itertools import count
from stable_baselines3 import PPO
from stable_baselines3.common.evaluation import evaluate_policy
from torch.utils.tensorboard import SummaryWriter


class PPOAgent(object):
    def __init__(self, chance_of_wind=0, random_grid=False, wind_strength=1, log=True, reward_won=1, reward_lost=-1,
                 verbose=0, name=""):
        self.name = name
        self.reward_won = reward_won
        self.reward_lost = reward_lost
        self.random_grid = random_grid
        self.chance_of_wind = chance_of_wind
        self.wind_strength = wind_strength
        self.env = cliffWalking.CliffGame(chance_of_wind=chance_of_wind, random_grid=random_grid, log=log,
                                          reward_won=reward_won, reward_lost=reward_lost, wind_strength=wind_strength,
                                          name=self.name)
        self.tensorboard = f"runsPPO/{self.name}"
        self.policy_kwargs = dict(activation_fn=torch.nn.ReLU,
                                  net_arch=[64, 32])  # default is [64,64], [64, 32] seems to be ideal
                                                      # Too big network can cause overfitting with random grid
        self.n_steps = 2048  # 2048
        self.batch_size = 128  # 128 for one layout, 64 for random
        self.lr = 0.0003  # 0.0003 seems fine
        self.clip_range = 0.2  # 0.1 - 0.3, 0.2 works fine
        self.gamma = 0.99  # 0.99 is good
        self.n_epochs = 15  # 3-30, 15 seems good
        self.ent_coef = 0.0  # 0.01 for random, probably lower for single grid

        if random_grid:
            self.n_steps = 4096  # 2048 or 4069 for random
            self.batch_size = 64  # 128 for one layout, 64 for random
            self.lr = 0.0003  # 0.0003 seems fine
            self.clip_range = 0.2  # 0.1 - 0.3, 0.2 works fine
            self.gamma = 0.99  # 0.99 is good
            self.n_epochs = 15  # 3-30, 15 seems good
            self.ent_coef = 0.01  # 0.01 for random, probably lower for single grid

        self.model = PPO('MlpPolicy', self.env, device="auto", n_steps=self.n_steps, batch_size=self.batch_size,
                         learning_rate=self.lr, clip_range=self.clip_range, gamma=self.gamma, n_epochs=self.n_epochs,
                         ent_coef=self.ent_coef, tensorboard_log=self.tensorboard, verbose=verbose)
        self.model.set_random_seed()
        self.logger = SummaryWriter(self.tensorboard)
        self.logger.add_text("Hyperparameters", f"""Steps: {self.n_steps}, Batch_size: {self.batch_size}, lr:{self.lr},
        Clip Range: {self.clip_range}, Gamma: {self.gamma}, Epochs: {self.n_epochs}, ent_coef: {self.ent_coef}""")

    def learn(self, timesteps):
        print("Starting training...")
        self.model.learn(total_timesteps=timesteps)
        print("Training finished")
        self.model.save(f"ModelsPPO/{self.name}.zip")
        self.env.close()

    def test(self):
        self.env = cliffWalking.CliffGame(chance_of_wind=self.chance_of_wind, random_grid=self.random_grid, render=True,
                                          reward_won=self.reward_won, reward_lost=self.reward_lost)
        done = False
        while not done:
            observation = self.env.reset()
            steps = 0
            for t in count():
                # Select and perform an action
                action, _ = self.model.predict(observation)
                print(action)
                observation, reward, done, _ = self.env.step(action)
                steps += 1

                # Observe new state
                if not done:
                    next_state = observation
                else:
                    next_state = None

                # Move to the next state
                observation = next_state

                if done:
                    break

        print(f"Testing final reward was: {reward} with {steps} steps")
        self.env.close()

    def test_nowind(self):
        self.env = cliffWalking.CliffGame(random_grid=self.random_grid, render=True, reward_won=self.reward_won,
                                          reward_lost=self.reward_lost)
        done = False
        while not done:
            observation = self.env.reset()
            steps = 0
            for t in count():
                # Select and perform an action
                action, _ = self.model.predict(observation, deterministic=True)
                print(action)
                observation, reward, done, _ = self.env.step(action)
                steps += 1

                # Observe new state
                if not done:
                    next_state = observation
                else:
                    next_state = None

                # Move to the next state
                observation = next_state

                if done:
                    break

        print(f"Testing final reward was: {reward} with {steps} steps")
        self.env.close()

    def evaluate_agent(self, episodes=100):
        env = cliffWalking.CliffGame(chance_of_wind=self.chance_of_wind, random_grid=self.random_grid, render=False,
                                     reward_won=self.reward_won, reward_lost=self.reward_lost)
        mean_reward, std_reward = evaluate_policy(self.model, env, n_eval_episodes=episodes)
        print(f"Mean reward: {mean_reward} +/- {std_reward}")
        env.close()

    def evaluate_agent_no_wind(self, episodes=100):
        env = cliffWalking.CliffGame(random_grid=self.random_grid, wind_strength=self.wind_strength,
                                     reward_won=self.reward_won, reward_lost=self.reward_lost)
        rewards, length = evaluate_policy(self.model, env, return_episode_rewards=True, n_eval_episodes=episodes)
        print(f"Rewards: {rewards} steps {length}")
        env.close()

    def evaluate_agent_test_grid(self, episodes=100):
        env = cliffWalking.CliffGame(chance_of_wind=self.chance_of_wind, test_grid=True, render=False,
                                     reward_won=self.reward_won, reward_lost=self.reward_lost)
        mean_reward, std_reward = evaluate_policy(self.model, env, n_eval_episodes=episodes)
        print(f"Mean reward: {mean_reward} +/- {std_reward}")
        env.close()

    def evaluate_agent_test_grid_no_wind(self, episodes=100):
        env = cliffWalking.CliffGame(test_grid=True, render=False, reward_won=self.reward_won,
                                     reward_lost=self.reward_lost)
        mean_reward, std_reward = evaluate_policy(self.model, env, n_eval_episodes=episodes)
        print(f"Mean reward: {mean_reward} +/- {std_reward}")
        env.close()


if __name__ == '__main__':
    agent = PPOAgent(chance_of_wind=17, random_grid=True, reward_won=1, reward_lost=-1, verbose=1)
    agent.learn(1_000_000)
    agent.test_nowind()
    agent.test()
    agent.evaluate_agent(200)
    agent.evaluate_agent_no_wind(200)
