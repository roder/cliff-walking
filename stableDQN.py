import cliffWalking
from itertools import count
import torch
from stable_baselines3 import DQN
from stable_baselines3.common.evaluation import evaluate_policy
from torch.utils.tensorboard import SummaryWriter


class DQNAgent(object):
    def __init__(self, chance_of_wind=0, random_grid=False, wind_strength=1, log=True, reward_won=1, reward_lost=-1,
                 verbose=0, name=""):
        self.name = name
        self.reward_won = reward_won
        self.reward_lost = reward_lost
        self.random_grid = random_grid
        self.chance_of_wind = chance_of_wind
        self.wind_strength = wind_strength
        self.env = cliffWalking.CliffGame(chance_of_wind=chance_of_wind, random_grid=random_grid, log=log,
                                          reward_lost=reward_lost, reward_won=reward_won, wind_strength=wind_strength,
                                          name=self.name)
        self.tensorboard = f"runsStableDQN/{self.name}"
        self.policy_kwargs = dict(activation_fn=torch.nn.ReLU,
                                  net_arch=[64, 32])  # Too big network can cause overfitting with random grid
        self.lr = 0.0003  # 0.000001 - 0.01
        self.buffer_size = 10_000
        self.learning_starts = 5_000  # 40-50% of buffer size
        self.batch_size = 128  # multiples of 64 -- 128 for one layout, 1024 for random
        self.gamma = 0.97  # 0.95-0.99 -- lower value to ensure it finds the fastest way 0.97 for random layout
        self.target_update = 100  # steps of 50s -- 100 for one layout, 250 for random
        self.exploration_fraction = 0.15  # 0.25 to 0.5; 0.3
        self.final_eps = 0.005
        self.verbose = verbose
        if random_grid:
            self.batch_size = 1024
            self.target_update = 250

        self.model = DQN('MlpPolicy', self.env, learning_rate=self.lr, learning_starts=self.learning_starts,
                         gamma=self.gamma, batch_size=self.batch_size, target_update_interval=self.target_update,
                         buffer_size=self.buffer_size, exploration_fraction=self.exploration_fraction,
                         exploration_final_eps=self.final_eps, device="auto", tensorboard_log=self.tensorboard,
                         verbose=self.verbose)
        self.model.set_random_seed()
        self.logger = SummaryWriter(self.tensorboard)
        self.logger.add_text("Hyperparameters", f"""LR: {self.lr}, Batch_size: {self.batch_size},
        Buffer Size: {self.buffer_size}, Gamma: {self.gamma}, learning starts: {self.learning_starts},
        Target Update: {self.target_update}, Exploration fraction: {self.exploration_fraction}""")

    def learn(self, timesteps):
        print("Starting training...")
        self.model.learn(total_timesteps=timesteps)
        print("Training finished")
        self.model.save(f"ModelsStableDQN/{self.name}.zip")
        self.env.close()

    def test(self):
        self.env = cliffWalking.CliffGame(chance_of_wind=self.chance_of_wind, random_grid=self.random_grid, render=True,
                                          wind_strength=self.wind_strength, reward_won=self.reward_won,
                                          reward_lost=self.reward_lost)
        done = False
        while not done:
            observation = self.env.reset()
            steps = 0
            for t in count():
                # Select and perform an action
                action, _ = self.model.predict(observation, deterministic=True)
                print(action)
                observation, reward, done, _ = self.env.step(action)
                steps += 1

                # Observe new state
                if not done:
                    next_state = observation
                else:
                    next_state = None

                # Move to the next state
                observation = next_state

                if done:
                    break

        print(f"Testing final reward was: {reward} with {steps} steps")
        self.env.close()

    def test_nowind(self):
        self.env = cliffWalking.CliffGame(random_grid=self.random_grid, render=True, reward_won=self.reward_won,
                                          reward_lost=self.reward_lost)
        done = False
        while not done:
            observation = self.env.reset()
            steps = 0
            for t in count():
                # Select and perform an action
                action, _ = self.model.predict(observation, deterministic=True)
                print(action)
                observation, reward, done, _ = self.env.step(action)
                steps += 1

                # Observe new state
                if not done:
                    next_state = observation
                else:
                    next_state = None

                # Move to the next state
                observation = next_state

                if done:
                    break

        print(f"Testing final reward was: {reward} with {steps} steps")
        self.env.close()

    def evaluate_agent(self, episodes=100):
        env = cliffWalking.CliffGame(chance_of_wind=self.chance_of_wind, random_grid=self.random_grid,
                                     wind_strength=self.wind_strength, reward_won=self.reward_won,
                                     reward_lost=self.reward_lost)
        mean_reward, std_reward = evaluate_policy(self.model, env, n_eval_episodes=episodes)
        print(f"Mean reward: {mean_reward} +/- {std_reward}")
        env.close()

    def evaluate_agent_no_wind(self, episodes=100):
        env = cliffWalking.CliffGame(random_grid=self.random_grid, wind_strength=self.wind_strength,
                                     reward_won=self.reward_won, reward_lost=self.reward_lost)
        rewards, length = evaluate_policy(self.model, env, return_episode_rewards=True, n_eval_episodes=episodes)
        print(f"Rewards: {rewards} steps {length}")
        env.close()

    def evaluate_agent_test_grid(self, episodes=100):
        env = cliffWalking.CliffGame(chance_of_wind=self.chance_of_wind, test_grid=True, render=False,
                                     reward_won=self.reward_won, reward_lost=self.reward_lost)
        mean_reward, std_reward = evaluate_policy(self.model, env, n_eval_episodes=episodes)
        print(f"Mean reward: {mean_reward} +/- {std_reward}")
        env.close()

    def evaluate_agent_test_grid_no_wind(self, episodes=100):
        env = cliffWalking.CliffGame(test_grid=True, render=False, reward_won=self.reward_won,
                                     reward_lost=self.reward_lost)
        mean_reward, std_reward = evaluate_policy(self.model, env, n_eval_episodes=episodes)
        print(f"Mean reward: {mean_reward} +/- {std_reward}")
        env.close()


if __name__ == '__main__':
    agent = DQNAgent(chance_of_wind=17, random_grid=False, log=True, reward_won=1, reward_lost=-1,
                     verbose=1, name="")
    agent.learn(200_000)
    agent.test()  # Random Grid = True
    agent.test_nowind()
    agent.evaluate_agent(200)
    agent.evaluate_agent_no_wind(200)
