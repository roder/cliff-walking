import matplotlib
import matplotlib.pyplot as plt
import pickle
import numpy as np
import pandas as pd

# matplotlib.rc('axes', titlesize=18)
matplotlib.rc('font', size=11)
matplotlib.rc('legend', fontsize=14)
matplotlib.rc('axes', labelsize=16)


def plot_heatmap(heatmap, i, j, episodes, algorithm, title=True):
    heatmap = heatmap / np.amax(heatmap)
    heatmap_plt = plt.figure()
    if title:
        lower = i/j * episodes
        upper = (i+1)/j * episodes
        heatmap_plt.suptitle(f"Heatmap {i+1}/{j} {algorithm}\nEpisoden {int(lower)} bis {int(upper)}")
        ax2 = heatmap_plt.add_axes([0.1, 0.1, 0.8, 0.8])
    else:
        ax2 = heatmap_plt.add_axes([0.1, 0.1, 0.85, 0.85])
    ax2.set_xlabel("X Koordinate")
    ax2.set_ylabel("Y Koordinate")
    ax2.set_ylim(-0.5, 5.5)
    ax2.set_xlim(-0.5, 5.5)
    plt.imshow(heatmap, cmap='BuPu', interpolation='nearest')
    # plt.colorbar()
    minor_ticks = np.linspace(-0.5, 5.5, 7)
    ax2.set_yticks(minor_ticks, minor=True)
    ax2.set_xticks(minor_ticks, minor=True)
    plt.grid(which="minor", alpha=0.5)
    plt.show()


def minimize(logs):
    min = logs[0].min
    for log in logs:
        if log.min < min:
            min = log.min

    for log in logs:
        log.wind_occurrences_arr = log.wind_occurrences_arr[:min]
        log.steps_arr = log.steps_arr[:min]
        log.success_arr = log.success_arr[:min]
        log.distance_to_cliff_arr = log.distance_to_cliff_arr[:min]

    return logs, min


class Data(object):
    def __init__(self, paths):

        self.numpaths = len(paths)

        self.wind_occurrences_arr = []
        self.steps_arr = []
        self.success_arr = []
        self.coordinates = []
        self.distance_to_cliff_arr = []
        self.min = 0

        for i in range(self.numpaths):
            file_name = open(f"pickles/{paths[i]}.pkl", "rb")
            dict_of_plots = pickle.load(file_name)
            self.wind_occurrences_arr.append(dict_of_plots["wind_occurrences_per_episode"])
            self.steps_arr.append(dict_of_plots["steps_per_episode"])
            self.success_arr.append(dict_of_plots["success_per_episode"])
            self.coordinates.append(dict_of_plots["coordinates"])
            self.distance_to_cliff_arr.append(dict_of_plots["distance_to_cliff"])

        self.wind_occurrences_arr = self.combine_arrays(self.cut_arrays(self.wind_occurrences_arr))
        self.steps_arr = self.combine_arrays(self.cut_arrays(self.steps_arr))
        self.distance_to_cliff_arr = self.combine_arrays(self.cut_arrays(self.distance_to_cliff_arr))
        self.success_arr = self.combine_arrays(self.cut_arrays(self.success_arr))

    def combine_arrays(self, array):
        avg_array = np.average(array, axis=0)

        return avg_array

    def cut_arrays(self, array):
        if self.numpaths > 1:
            minim = len(array[0])
            for x in array:
                if len(x) < minim:
                    minim = len(x)

            for i in range(len(array)):
                array[i] = array[i][:minim]

            self.min = minim
        return array


class Plotter(object):
    def __init__(self, paths, titles=False, cut=False):
        self.paths = paths
        self.num_of_plots = len(paths)

        self.titles = titles

        self.logs = []
        self.labels = []
        self.colors = [
            "tab:blue",
            "tab:orange",
            "tab:cyan",
            "tab:purple",
            "tab:olive",
            "tab:gray"
        ]
        for i in range(self.num_of_plots):
            data = Data(paths[i][1])
            self.logs.append(data)
            self.labels.append(self.paths[i][0])

        if cut:
            self.logs, self.min = minimize(self.logs)

    def plot_wind(self):
        # Plot wind occurrence
        wind_plt = plt.figure()
        if self.titles:
            wind_plt.suptitle("Windhäufigkeit pro Episode")
            ax1 = wind_plt.add_axes([0.1, 0.1, 0.8, 0.8])
        else:
            ax1 = wind_plt.add_axes([0.1, 0.1, 0.85, 0.85])

        ax1.set_xlabel("Episode")
        ax1.set_ylabel("Windhäufigkeit")

        n_steps = 100

        ax1.grid(color='grey', linewidth=0.5)

        n = 0
        for log in self.logs:
            time_series = pd.DataFrame(log.wind_occurrences_arr)
            average = time_series.rolling(n_steps).mean()
            std = time_series.rolling(n_steps).std()

            under = (average - std)[0]
            over = (average + std)[0]

            eps = range(len(average))

            ax1.plot(average, color=self.colors[n], linewidth=2, label=self.labels[n])
            ax1.fill_between(eps, under, over, color=self.colors[n], alpha=0.2)
            if self.num_of_plots > 1:
                ax1.legend()
            n += 1

        ax1.set_ylim(bottom=0)
        ax1.set_xlim(left=0, right=self.min)

    def heatmap(self, splits=3):
        n = 0
        coordinates = [[]]*splits
        for log in self.logs:
            for coords in log.coordinates:
                for i in range(splits):
                    coordinates[i].append(np.array_split(coords, splits)[i])
            for j in range(splits):
                heatmap = np.zeros((6, 6), dtype=int)
                for coords in coordinates[j]:
                    for i in coords:
                        player_y = i[0]
                        player_x = i[1]
                        heatmap[5 - player_y][player_x] += 1
                plot_heatmap(heatmap, j, splits, log.min, self.labels[n], title=self.titles)

            n += 1

    def plot_success(self):
        # Plot success
        success_plt = plt.figure()
        if self.titles:
            success_plt.suptitle("Erfolg pro Episode")
            ax2 = success_plt.add_axes([0.1, 0.1, 0.8, 0.8])
        else:
            ax2 = success_plt.add_axes([0.1, 0.1, 0.85, 0.85])

        ax2.set_xlabel("Episode")
        ax2.set_ylabel("Erfolg")

        n_steps = 100

        ax2.grid(color='grey', linewidth=0.5)

        n = 0
        for log in self.logs:
            time_series = pd.DataFrame(log.success_arr)
            average = time_series.rolling(n_steps).mean()
            std = time_series.rolling(n_steps).std()

            under = (average - std)[0]
            over = (average + std)[0]

            eps = range(len(average))

            ax2.plot(average, color=self.colors[n], linewidth=2, label=self.labels[n])
            ax2.fill_between(eps, under, over, color=self.colors[n], alpha=0.2)
            if self.num_of_plots > 1:
                ax2.legend()
            n += 1

        ax2.set_ylim(bottom=0)
        ax2.set_xlim(left=0, right=self.min)

    def plot_distance(self):
        # Plot success
        distance_plt = plt.figure()
        if self.titles:
            distance_plt.suptitle("Distanz zur Klippe pro Episode")
            ax3 = distance_plt.add_axes([0.1, 0.1, 0.8, 0.8])
        else:
            ax3 = distance_plt.add_axes([0.1, 0.1, 0.85, 0.85])

        ax3.set_xlabel("Episode")
        ax3.set_ylabel("Distanz zur Klippe")

        n_steps = 100

        ax3.grid(color='grey', linewidth=0.5)

        n = 0
        for log in self.logs:
            time_series = pd.DataFrame(log.distance_to_cliff_arr)
            average = time_series.rolling(n_steps).mean()
            std = time_series.rolling(n_steps).std()

            under = (average - std)[0]
            over = (average + std)[0]

            eps = range(len(average))

            ax3.plot(average, color=self.colors[n], linewidth=2, label=self.labels[n])
            ax3.fill_between(eps, under, over, color=self.colors[n], alpha=0.2)
            n += 1

        ax3.axhline(y=1, color='red', linewidth=2, label="Unsicher")
        ax3.axhline(y=2, color='green', linewidth=2, label="Sicher")
        ax3.legend()

        ax3.set_ylim([0, 5])
        ax3.set_xlim(left=0, right=self.min)

    def plot_steps(self):
        # Plot success
        steps_plt = plt.figure()
        if self.titles:
            steps_plt.suptitle("Schritte pro Episode")
            ax4 = steps_plt.add_axes([0.1, 0.1, 0.8, 0.8])
        else:
            ax4 = steps_plt.add_axes([0.1, 0.1, 0.85, 0.85])

        ax4.set_xlabel("Episode")
        ax4.set_ylabel("Schritte")

        n_steps = 100

        ax4.grid(color='grey', linewidth=0.5)

        n = 0
        for log in self.logs:
            time_series = pd.DataFrame(log.steps_arr)
            average = time_series.rolling(n_steps).mean()
            std = time_series.rolling(n_steps).std()

            under = (average - std)[0]
            over = (average + std)[0]

            eps = range(len(average))

            ax4.plot(average, color=self.colors[n], linewidth=2, label=self.labels[n])
            ax4.fill_between(eps, under, over, color=self.colors[n], alpha=0.2)
            if self.num_of_plots > 1:
                ax4.legend()
            n += 1

        ax4.set_ylim(bottom=0)
        ax4.set_xlim(left=0, right=self.min)


if __name__ == '__main__':
    plots = [
        ["DQN",
         ["DQN_random_6of8_1", "DQN_random_6of8_2", "DQN_random_6of8_3",
          "DQN_random_6of8_4", "DQN_random_6of8_5", "DQN_random_6of8_6",
          "DQN_random_6of8_7", "DQN_random_6of8_8", "DQN_random_6of8_9",
          "DQN_random_6of8_10", "DQN_random_6of8_11", "DQN_random_6of8_12"]],
        ["PPO",
         ["PPO_random_6of8_1", "PPO_random_6of8_2", "PPO_random_6of8_3",
          "PPO_random_6of8_4", "PPO_random_6of8_5", "PPO_random_6of8_6",
          "PPO_random_6of8_7", "PPO_random_6of8_8", "PPO_random_6of8_9",
          "PPO_random_6of8_10", "PPO_random_6of8_11", "PPO_random_6of8_12"]],
    ]
    plotter = Plotter(plots, titles=False, cut=True)
    plotter.heatmap(splits=2)
    plotter.plot_wind()
    plotter.plot_steps()
    plotter.plot_success()
    plotter.plot_distance()
