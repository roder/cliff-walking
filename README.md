# Cliff Walking Project
[link to this repo](https://gitlab2.cip.ifi.lmu.de/roder/cliff-walking)
### Packages
All necessary packages can be installed via `pip -r "requirements.txt"`, it is common practice to create a virtual env for that.
Refer to [This guide](https://docs.python.org/3/tutorial/venv.html)


### File Documentation
*cliffWalking.py*\
The environment, can be used for training agents or be played as a human. Has a few configuration options. Execute the main to play as a human. Is gym compatible for training RL algorithms.
- "render" (bool): determines whether the environment is rendered or not, not recommended for training RL algorithms.
- "chance_of_wind" (int): the chance of wind in %, default is 0
- "random_grid" (bool): whether to use 6 out of 8 random grids or a single one
- "test_grid" (bool): whether to use the 2 remaining test grid for agents that were trained on random grids
- "wind_strength" (int): can be 1 or 2, how many steps the agent is pushed towards the cliff by the wind
- "name" (str): name of the agent, used for logging and saving data
- "log" (bool): whether to log the data from within the env, will be saved to "name.pkl" in the subfolder pickles
- "reward_won" / "reward_lost" (int): specify the rewards

*stablePPO.py*\
Uses stablebaslines3 PPO, agents can be configured with similar arguments as the cliffWalking env and will create an instance of the env to train on.
Has learn function to train agent.
Test function will call the env with rendering so the behavior of the agent can be seen.
Will save models to /ModelsPPO

*stableDQN.py*\
Same as stablePPO.py just for DQN

*evaluation.py*\
Has an Evaluator class which can be used to set chance of wind and wind strenght.
Can only evaluate DQN or PPO models at once.
The parameter dqn or ppo needs to be set accordingly
The names of the models, which should be saved in /ModelsPPO or /ModelsStableDQN, need to be given as an array.
Has four evaluation methods, with wind, without wind, random grid and test grid.
All methods will create the corresponding env and run the desired amount of episodes, logging the values.

*plotTraining.py*\
Used to plot the training values.
Titles can be turned on and off.
Logs can be cut at the end to be same length, to create cleaner graphs.
Paths need to be passed in according format:
`paths = [
	["Title1", ["example1.pkl", "example2.pkl",...]],
	["Title2", ["example3.pkl", "example4.pkl",...]]
]`

*plotEvaluation.py*\
Used to create bar charts or boxplots of the evaluation.
Paths same format as in plotTraining.py

*waffleChart.py*\
Used to make waffleCharts of the rewards/success.
Paths same format as in plotTraining.py

*scheduler.py*\
Can be used to train multiple Agents one after another.

### remaining info
The two RL algorithms PPO and DQN will also use tensorboard to log the data. These runs can be found and viewed from /runsPPO and /runsStableDQN\
In a command line from the root dir run eg. `tensorboard --logdir=runsPPO`\

The evaluation methods in stablePPO.py and stableDQN.py differ from the methods in evaluaiton.py, they don't log the results and are just meant for a quick check after the training.
For a proper evaluation use evaluation.py
